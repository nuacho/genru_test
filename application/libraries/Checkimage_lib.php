<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkimage_lib
{
	private $CI;
	private $chunk = 50;
	private $cursor = 0;
	private $image_table = 'images';
	private $image_folder = 'user_guide/_images/product_image/';
	private $bugs = [];

	public function __construct(array $params = [])
	{
		$this->chunk = $params['chunk']??$this->chunk;
		$this->image_table = $params['image_table']??$this->image_table;
		$this->image_folder = $params['image_folder']??$this->image_folder;

		$this->CI =& get_instance();
		$this->CI->load->helper('file');
		$this->CI->load->database($this->CI->config->item('db'));
		$this->CI->load->model('image_model');
		$this->CI->load->model('bug_model');
	}

	/**
	 * Start check
	 * @return void
	 */
	public function check () : void
	{
		$this->truncate_bug();

		while (!empty($rows = $this->get_chunk())) {
			foreach ($rows as $row) {
				if ($this->check_image($row->image_default) === false) {
					$this->bug($row, 'default');
				}
				if ($this->check_image($row->image_big) === false) {
					$this->bug($row, 'big');
				}
				if ($this->check_image($row->image_small) === false) {
					$this->bug($row, 'small');
				}
			}
			$this->cursor += $this->chunk;
		}
		$this->check_free_bugs();
	}

	/**
	 * Get a batch data
	 * @return array
	 */
	private function get_chunk () : array
	{
		return $this->CI->image_model->get_chunk_entries($this->cursor, $this->chunk);
	}

	/**
	 * Set size of chunk
	 * @param int $set
	 * @return void
	 */
	public function set_chunk (int $set) : void
	{
		$this->chunk = $set;
	}

	/**
	 * Checking the existence and size of the image
	 * @param string $image
	 * @return bool
	 */
	private function check_image (string $image) : bool
	{
		$explode_image = explode('/', $image);
		if (count($explode_image) > 1) {
			$dir_image = $explode_image[0];
		}
		else {
			/*
			 * т.к. все изображения находятся в подпапках, это исключение не реализовано
			 */
			return FALSE;
		}

		$file_info = get_file_info($this->image_folder.$image,['size']);

		if (empty($file_info) || $file_info['size'] < 1) {
			return FALSE;
		}
		return TRUE;
	}

	/**
	 * Add bug to array data
	 * @param object $image
	 * @param string $type
	 * @return void
	 */
	private function bug (object $image, string $type) : void
	{
		$data = [
			'image_id' => $image->id,
			'image_type' => $type
		];
		$this->bugs[] = $data;

		if (count($this->bugs) >= 100) {
			$this->CI->bug_model->insert_bugs($this->bugs);
			$this->bugs = [];
		}
	}

	/**
	 * Check data for void
	 */
	private function check_free_bugs () : void
	{
		if (!empty($this->bugs)) {
			$this->CI->bug_model->insert_bugs($this->bugs);
		}
	}

	/**
	 * Clear a bugs table
	 * @return void
	 */
	private function truncate_bug () : void
	{
		$this->CI->bug_model->truncate_bugs();
	}

}
