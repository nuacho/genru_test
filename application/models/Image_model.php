<?php
class Image_model extends CI_Model {

	public $product_id;
	public $image_default;
	public $image_big;
	public $image_small;

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Select a batch data
	 * @param int $start
	 * @param int $offset
	 * @return array
	 */
	public function get_chunk_entries(int $start, int $offset) : array
	{
		$query = $this->db->get('images', $offset, $start);
		return $query->result();
	}
}
