<?php
class Bug_model extends CI_Model {

	public $image_id;
	public $image_type;

	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * insert a batch of data to DB
	 * @param array $data
	 * @return void
	 */
	public function insert_bugs (array $data) : void
	{
		$this->db->insert_batch('bugs', $data);
	}

	/**
	 * Clear a bugs table
	 * @return void
	 */
	public function truncate_bugs () : void
	{
		$this->db->truncate('bugs');
	}
}
