<?php

class Migration_Images extends CI_Migration {

	private $table = 'images';

    public function up() {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => TRUE
            ),
			'product_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE
			),
			'image_default' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE
			),
			'image_big' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE
			),
			'image_small' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE
			),
			'storage' => array(
				'type' => 'VARCHAR',
				'constraint' => 100,
				'default' => 'local'
			),
			'is_main' => array(
				'type' => 'TINYINT',
				'constraint' => 1,
				'null' => FALSE,
				'default' => 0
			),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('images');


		$limit = 60000;
		echo "seeding $limit rows";
		echo PHP_EOL;

		for ($i = 0; $i < $limit; $i++) {
			if ($i % 1000 === 0 && $i !== 0) {
				echo $i . ' rows make';
				echo PHP_EOL;
			}

			$img_name = $this->faker->word . '_' . $this->faker->firstNameMale;
			$img_folder = mt_rand(0,100);

			if (!is_dir('user_guide/_images/product_image/' . $img_folder)) {
				mkdir('./user_guide/_images/product_image/' . $img_folder, 0777, TRUE);
			}

			$data = array(
				'product_id' => 1,
				'image_default' => $img_folder . '/' . $img_name . '_200x200.jpg',
				'image_big' => $img_folder . '/' . $img_name . '_1000x1000.jpg',
				'image_small' => $img_folder . '/' . $img_name . '_100x100.jpg',
				'storage' => 'local',
				'is_main' => 1,
			);

			$this->db->insert($this->table, $data);

			$is_created = mt_rand(0,1);
			$is_empty = mt_rand(0,100);

			if ($is_created === 1) {
				write_file('./user_guide/_images/product_image/' . $img_folder . '/' . $img_name . '_200x200.jpg', ($is_empty < 2)?"":"1");
				write_file('./user_guide/_images/product_image/' . $img_folder . '/' . $img_name . '_1000x1000.jpg', "1");
				write_file('./user_guide/_images/product_image/' . $img_folder . '/' . $img_name . '_100x100.jpg', "1");

				if ($is_empty < 2) {
					echo "file " . './user_guide/_images/product_image/' . $img_folder . '/' . $img_name . '_200x200.jpg' . ' created is empty';
					echo PHP_EOL;
				}
			}
		}

		echo $limit . ' rows make';
		echo PHP_EOL;
    }

    public function down() {
        $this->dbforge->drop_table('images');
    }

}
