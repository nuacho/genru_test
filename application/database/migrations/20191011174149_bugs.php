<?php

class Migration_bugs extends CI_Migration {
	private $table = 'bugs';

    public function up() {
        $this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => TRUE
			),
			'image_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'null' => TRUE
			),
			'image_type' => array(
				'type' => 'VARCHAR',
				'constraint' => 255,
				'null' => TRUE
			),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('bugs');

		$this->db->query('ALTER TABLE ' . $this->table . ' ADD FOREIGN KEY(`image_id`) REFERENCES `images` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;');
    }

    public function down() {
        $this->dbforge->drop_table('bugs');
    }

}
