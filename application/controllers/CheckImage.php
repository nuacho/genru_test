<?php
class Checkimage extends CI_Controller {

	public function check()
	{
		memory_get_usage();
		echo "Start check".PHP_EOL;

		$params = [
			'image_table' => 'images',
			'image_folder' => 'user_guide/_images/product_image/',
			'chunk' => 100
		];
		$this->load->library('checkimage_lib', $params);

		$this->checkimage_lib->set_chunk(500);
		$this->checkimage_lib->check();

		echo PHP_EOL.PHP_EOL;
		$memory = (!function_exists('memory_get_usage')) ? '' : round(memory_get_usage()/1024/1024, 2) . 'MB';
		echo 'Used memory: ' . $memory;

		echo PHP_EOL;
		$dat = getrusage();
		echo 'Seconds: ' . $dat["ru_utime.tv_sec"];
	}
}
